describe('gitlab.io page status test', function () {
    it('open website', function () {
        cy.visit('https://pzpp.gitlab.io/projkektor/#5')
    })
});

describe('website content test', function () {
    it('website contains title and broadcast options', function () {
        cy.visit('https://pzpp.gitlab.io/projkektor/#5');
        cy.contains('ProjKEKtor');
        cy.get('label').contains("Screen");
        cy.get('label').contains("Audio + Video");
        cy.get('label').contains("Only Audio");
    });
});

describe('Broadcast test', function () {
    it('broadcast starts after button is clicked', function () {
        //Problem related to ResizeObserver loop
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        cy.visit('https://pzpp.gitlab.io/projkektor/#5');
        cy.contains("Setup New Broadcast").click();
        // cy.get('video')
    });
});

describe('stop Broadcast test', function () {
    it('broadcast stopped after button is clicked', function () {
        //Problem related to ResizeObserver loop
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        cy.visit('https://pzpp.gitlab.io/projkektor/#5');
		cy.contains("Setup New Broadcast").click();
        cy.contains("Stop Broadcast").click();
        cy.get('video').should('not.exist')
    });
});

describe('select only audio Broadcast test', function () {
    it('broadcast stopped after button is clicked', function () {
        //Problem related to ResizeObserver loop
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        cy.visit('https://pzpp.gitlab.io/projkektor/#5');
		cy.get('label').contains("Only Audio").click();
		cy.contains("Setup New Broadcast").click();
        cy.get('video').should('not.exist')
    });
});
