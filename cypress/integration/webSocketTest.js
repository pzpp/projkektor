describe('Socket.io connectivity test', function () {

    it('Check if server is online', function () {
        cy.visit('https://pzpp.gitlab.io/projkektor/#5');
        cy.request('https://socketio-over-nodejs2.herokuapp.com/socket.io/1/').should((response) => {
            expect(response.status).to.eq(200);
            expect(response.body).to.contain('xhr-polling,jsonp-polling');
        });
    }
    );
    it('Check if server accepts our ID', function () {
        cy.request('https://socketio-over-nodejs2.herokuapp.com/socket.io/1/xhr-polling/K-W_pmS9JYyUsUTHqodX').
            should((response) => {
                expect(response.status).to.eq(200);
                expect(response.body).to.contain('::1');
            });
    }
    );
    it('Check if server accepts his ID', function () {
        cy.request('https://socketio-over-nodejs2.herokuapp.com/socket.io/1/').
            should((response) => {
                expect(response.status).to.eq(200);
            }).then((response) => {
                var id = response.body.split(":")[0]
                cy.request(
                    'https://socketio-over-nodejs2.herokuapp.com/socket.io/1/xhr-polling/' + id).
                    should((response) => {
                        expect(response.status).to.eq(200);
                        expect(response.body).to.contain('1::');
                    });
            });
    }
    );

});
